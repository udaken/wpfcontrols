using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace System.Common.Highliter
{
  public interface ISqlFormatProvider : IDisposable
  {

    /// <summary>
    /// initialize formatprovider buffer
    /// </summary>
    /// <param name="source">source string</param>
    /// <param name="wordOnly">if true simple compare token no delimiter char</param>
    void SetText( string source, bool wordOnly = true );


    /// <summary>
    /// Add to internal list then table names
    /// </summary>
    /// <param name="tableList">coma separated tablenames</param>
    void AddTables( string tableList );

    /// <summary>
    /// Add simple tablename to internal list
    /// </summary>
    /// <param name="tablename">one name of table</param>
    void AddTable( string tablename );

    /// <summary>
    /// Add to internal list the view names
    /// </summary>
    /// <param name="viewList">coma separated viewnames</param>
    void AddViews( string viewList );

    /// <summary>
    /// add simple view to internal list
    /// </summary>
    /// <param name="viewName">simple viewName</param>
    void AddView( string viewName );

    /// <summary>
    /// Add to internal list then domain names
    /// </summary>
    /// <param name="domainList">coma separated domainnames</param>
    void AddDomains( string domainList );

    /// <summary>
    /// Add one domain name to internal list
    /// </summary>
    /// <param name="domainName"></param>
    void AddDomain( string domainName );


    /// <summary>
    /// end of source, no more processable char
    /// </summary>
    bool Eof { get; }

    /// <summary>
    /// Curent token type
    /// </summary>
    TokenType TokenId { get; }

    /// <summary>
    /// current token
    /// </summary>
    string Token { get; }


    /// <summary>
    /// Process next token
    /// </summary>
    void Next();
  }
}