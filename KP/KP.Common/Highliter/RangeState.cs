﻿// Solution: TESZT
// Project: WpfApplication2
// FileName: RangeState.cs
// Created: 2015.07.13 9:41
// Last format: 2015.07.13 9:41
// UserName: kp

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace System.Common.Highliter
{
  public enum RangeState
  {
    Unknown, 
    Comment, 
    String, 
    ConditionalComment
  }
}