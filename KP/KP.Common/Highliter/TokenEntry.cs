﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace System.Common.Highliter
{
  public class TokenEntry
  {
    public TokenEntry( string keyword, TokenType type )
    {
      KeyLen = keyword.Length;
      Keyword = keyword;
      TokenType = type;
    }

    public int KeyLen { get; private set; }
    public string Keyword { get; private set; }
    public TokenType TokenType { get; private set; }

    public override int GetHashCode()
    {
      return Keyword.ToUpper().Trim().GetHashCode();
    }

  }
}