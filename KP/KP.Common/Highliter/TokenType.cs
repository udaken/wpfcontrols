﻿// Solution: TESZT
// Project: WpfApplication2
// FileName: TokenType.cs
// Created: 2015.07.13 9:09
// Last format: 2015.07.13 9:10
// UserName: kp

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace System.Common.Highliter
{
  [Flags]
  public enum TokenType
  {
    Unknown = 1,
    Comment = 2 * Unknown,
    Datatype = 2 * Comment,
    DefaultPackage = 2 * Datatype,
    Exception = 2 * DefaultPackage,
    Function = 2 * Exception,
    Identifier = 2 * Function,
    KeyWord = 2 * Identifier,
    Null = 2 * KeyWord,
    Number = 2 * Null,
    Space = 2 * Number,
    CRLF = 2 * Space,
    PLSQL = 2 * CRLF,
    SQLPlus = 2 * PLSQL,
    String = 2 * SQLPlus,
    Symbol = 2 * String,
    TableName = 2 * Symbol,
    ViewName = 2 * TableName,
    Domain = 2 * ViewName,
    Variable = 2 * Domain,
    ConditionalComment = 2 * Variable,
    DelimitedIdentifier = 2 * ConditionalComment



  }
}
