﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace System.Common.Lock
{
  public class ObservableMonitor : SimpleMonitor
  {
    private event EventHandler Events;
    protected virtual void FireEvents()
    {
      var handler = Events;

      if (handler != null)
        handler.Invoke(this, EventArgs.Empty);
    }

    public void Subscribe(EventHandler target)
    {
      Events += target;
    }

    #region Overrides of SimpleMonitor

    protected override void Dispose(bool disposing)
    {
      if (!Busy)
        return;

      base.Dispose(disposing);
      if (!Busy)
        FireEvents();
    }

    #endregion
  }
}
