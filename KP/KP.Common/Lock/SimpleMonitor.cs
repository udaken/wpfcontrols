﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading;

namespace System.Common.Lock
{
  [DebuggerStepThrough]
  public class SimpleMonitor : IDisposable
  {
    private long fCount;

    public void Enter()
    {
      Interlocked.Increment(ref fCount);
    }

    public bool Busy
    {
      get { return Interlocked.Read( ref fCount ) > 0; } 
    }

    public void Dispose()
    {
      Dispose(true);
    }

    protected virtual void Dispose(bool disposing)
    {
      if ( !Busy )
        return;

        if (disposing)
      {
          Interlocked.Decrement(ref fCount);
      }

    }

  }
}
