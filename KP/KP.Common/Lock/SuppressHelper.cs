﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;

namespace System.Common.Lock
{
  [DebuggerStepThrough]
  internal sealed class SuppressHelper : IDisposable
  {
    private readonly Suppressor fOwner;

    internal SuppressHelper( Suppressor owner )
    {
      fOwner = owner;
      owner.Suppressed = true;
    }

    void IDisposable.Dispose()
    {
      fOwner.Suppressed = false;
    }
  }

}
