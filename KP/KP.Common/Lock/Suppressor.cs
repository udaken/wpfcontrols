﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace System.Common.Lock
{
  public sealed class Suppressor
  {
    public bool Suppressed { get; internal set; }

    public IDisposable Suppress()
    {
      return new SuppressHelper( this );
    }
  }
}
