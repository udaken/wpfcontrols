﻿using System;
using System.Collections.Generic;
using System.Diagnostics.CodeAnalysis;
using System.Linq;
using System.Text;

namespace System.Common.Native.Data
{
  [SuppressMessage( "ReSharper", "InconsistentNaming" )]
  [SuppressMessage( "ReSharper", "EnumUnderlyingTypeIsInt" )]
  public enum ABMsg : int
  {
    ABM_NEW=0,
    ABM_REMOVE,
    ABM_QUERYPOS,
    ABM_SETPOS,
    ABM_GETSTATE,
    ABM_GETTASKBARPOS,
    ABM_ACTIVATE,
    ABM_GETAUTOHIDEBAR,
    ABM_SETAUTOHIDEBAR,
    ABM_WINDOWPOSCHANGED,
    ABM_SETSTATE
  }
}
