﻿using System;
using System.Collections.Generic;
using System.Diagnostics.CodeAnalysis;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;

namespace System.Common.Native.Data
{
  [SuppressMessage( "ReSharper", "InconsistentNaming" )]
  [SuppressMessage( "ReSharper", "EnumUnderlyingTypeIsInt" )]
  [SuppressMessage( "ReSharper", "FieldCanBeMadeReadOnly.Global" )]
  [StructLayout( LayoutKind.Sequential )]
  public struct APPBARDATA
  {
    public int cbSize;
    public IntPtr hWnd;
    public int uCallbackMessage;
    public int uEdge;
    public RECT rc;
    public IntPtr lParam;
  }
}
