﻿using System;
using System.Collections.Generic;
using System.Diagnostics.CodeAnalysis;
using System.Linq;
using System.Text;

namespace System.Common.Native.Data
{
  [SuppressMessage( "ReSharper", "InconsistentNaming" )]
  public static class COMConst
  {
    public const int OLE_E_ADVISENOTSUPPORTED = unchecked( (int)0x80040003 );

    public const int DV_E_FORMATETC = unchecked( (int)0x80040064 );
    public const int DV_E_TYMED = unchecked( (int)0x80040069 );
    public const int DV_E_CLIPFORMAT = unchecked( (int)0x8004006A );
    public const int DV_E_DVASPECT = unchecked( (int)0x8004006B );

    public const int S_FALSE = 1;
    public const int S_OK    = 0;


  }
}
