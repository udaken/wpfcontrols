﻿using System;
using System.Collections.Generic;
using System.Diagnostics.CodeAnalysis;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;

namespace System.Common.Native.Data
{
  [SuppressMessage( "ReSharper", "InconsistentNaming" )]
  [StructLayout( LayoutKind.Sequential, CharSet = CharSet.Ansi )]
  public class DOCINFOA
  {
    [MarshalAs( UnmanagedType.LPStr )]
    public string pDocName;
    [MarshalAs( UnmanagedType.LPStr )]
    public string pOutputFile;
    [MarshalAs( UnmanagedType.LPStr )]
    public string pDataType;
  }
}
