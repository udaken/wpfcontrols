﻿using System;
using System.Collections.Generic;
using System.Diagnostics.CodeAnalysis;
using System.Linq;
using System.Text;

namespace System.Common.Native.Data
{
  [SuppressMessage( "ReSharper", "InconsistentNaming" )]
  [SuppressMessage( "ReSharper", "EnumUnderlyingTypeIsInt" )]
  public enum HookType : int
  {
    WH_MSGFILTER = -1,
    /*
     * LRESULT CALLBACK MessageProc(  _In_  int code,  WPARAM wParam,  _In_  LPARAM lParam );
     */
    WH_JOURNALRECORD = 0,
    /*
     * LRESULT CALLBACK JournalRecordProc( _In_  int code,  WPARAM wParam,  _In_  LPARAM lParam);
     */
    WH_JOURNALPLAYBACK = 1,
    /*
     * LRESULT CALLBACK JournalRecordProc( _In_  int code,  WPARAM wParam,  _In_  LPARAM lParam);
     */
    WH_KEYBOARD = 2,
    /*
     * LRESULT CALLBACK KeyboardProc(  _In_  int code,  _In_  WPARAM wParam,  _In_  LPARAM lParam );
     */
    WH_GETMESSAGE = 3,
    /*
     * LRESULT CALLBACK GetMsgProc(  _In_  int code,  _In_  WPARAM wParam,  _In_  LPARAM lParam );
     */
    WH_CALLWNDPROC = 4,
    /*
     * LRESULT CALLBACK CallWndProc(  _In_  int nCode,  _In_  WPARAM wParam,  _In_  LPARAM lParam );
     */
    WH_CBT = 5,
    /*
     * LRESULT CALLBACK CBTProc(  _In_  int nCode,  _In_  WPARAM wParam,  _In_  LPARAM lParam );
     */
    WH_SYSMSGFILTER = 6,
    /*
     * LRESULT CALLBACK SysMsgProc(  _In_  int nCode,  WPARAM wParam,  _In_  LPARAM lParam );
     */
    WH_MOUSE = 7,
    /*
     * LRESULT CALLBACK MouseProc(  _In_  int nCode,  _In_  WPARAM wParam,  _In_  LPARAM lParam );
     */
    WH_DEBUG = 9,
    /*
     * LRESULT CALLBACK DebugProc(  _In_  int nCode,  _In_  WPARAM wParam,  _In_  LPARAM lParam );
     */
    WH_FOREGROUNDIDLE = 11,
    /*
     * DWORD CALLBACK ForegroundIdleProc(  _In_  int code,  DWORD wParam,  LONG lParam );
     */
    WH_CALLWNDPROCRET = 12,
    /*
     * LRESULT CALLBACK CallWndRetProc(  _In_  int nCode,  _In_  WPARAM wParam,  _In_  LPARAM lParam );
     */
    WH_KEYBOARD_LL = 13,
    /*
     * LRESULT CALLBACK LowLevelKeyboardProc(  _In_  int nCode,  _In_  WPARAM wParam,  _In_  LPARAM lParam );
     */
    WH_MOUSE_LL = 14
    /*
     * LRESULT CALLBACK LowLevelMouseProc(  _In_  int nCode,  _In_  WPARAM wParam,  _In_  LPARAM lParam );
     */
  }
}
