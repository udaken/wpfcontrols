﻿using System;
using System.Collections.Generic;
using System.Diagnostics.CodeAnalysis;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;

namespace System.Common.Native.Data
{
  [SuppressMessage( "ReSharper", "InconsistentNaming" )]
  [SuppressMessage( "ReSharper", "EnumUnderlyingTypeIsInt" )]
  [SuppressMessage( "ReSharper", "FieldCanBeMadeReadOnly.Global" )]
  [StructLayout( LayoutKind.Sequential )]
  public struct KBDLLHOOKSTRUCT
  {
    public int vkCode;
    public int scanCode;
    public int flags;
    public int time;
    public IntPtr dwExtraInfo;
  }
}
