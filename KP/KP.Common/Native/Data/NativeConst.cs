﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security;
using System.Text;

namespace System.Common.Native.Data
{
  [SuppressUnmanagedCodeSecurity]
  public static class NativeConst
  {

    public const Int32 GWL_EXSTYLE = -20;
    public const Int32 WS_EX_LAYERED = 0x00080000;
    public const Int32 WS_EX_TRANSPARENT = 0x00000020;

    public const int WH_JOURNALRECORD   =  0;
    public const int WH_JOURNALPLAYBACK =  1;
    public const int WH_KEYBOARD        =  2;
    public const int WH_GETMESSAGE      =  3;
    public const int WH_CALLWNDPROC     =  4;
    public const int WH_CBT             =  5;
    public const int WH_SYSMSGFILTER    =  6;
    public const int WH_MOUSE           =  7;
    public const int WH_HARDWARE        =  8;
    public const int WH_DEBUG           =  9;
    public const int WH_SHELL           = 10;
    public const int WH_FOREGROUNDIDLE  = 11;
    public const int WH_CALLWNDPROCRET  = 12;
    public const int WH_KEYBOARD_LL     = 13;
    public const int WH_MOUSE_LL        = 14;

    public const int WM_KEYDOWN         = 0x0100;
    public const int WM_KEYUP           = 0x0101;
    public const int WM_CHAR            = 0x0102;
    public const int WM_SYSKEYDOWN      = 0x0104;
    public const int WM_SYSKEYUP        = 0x0105;
    public const int WM_SYSCHAR         = 0x0106;

    public const int VK_SHIFT           = 0x10;
    public const int VK_CONTROL         = 0x11;
    public const int VK_MENU            = 0x12;
    public const int VK_CAPITAL         = 0x14;

    public const int SW_HIDE            = 0;
    public const int SW_SHOW            = 1;


  }
}
