﻿using System;
using System.Collections.Generic;
using System.Diagnostics.CodeAnalysis;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;
using System.Windows;

namespace System.Common.Native.Data
{
  [StructLayout( LayoutKind.Sequential )]
  [SuppressMessage( "ReSharper", "InconsistentNaming" )]
  [SuppressMessage( "ReSharper", "FieldCanBeMadeReadOnly.Global" )]
  public struct POINT
  {
    public int X;
    public int Y;

    public POINT( int x, int y )
    {
      X = x;
      Y = y;
    }
    public POINT( Point pt )
    {
      X = Convert.ToInt32( pt.X );
      Y = Convert.ToInt32( pt.Y );
    }
  };
}
