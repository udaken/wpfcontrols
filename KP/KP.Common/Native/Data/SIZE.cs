﻿using System;
using System.Collections.Generic;
using System.Diagnostics.CodeAnalysis;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;

namespace System.Common.Native.Data
{
  [SuppressMessage( "ReSharper", "InconsistentNaming" )]
  [SuppressMessage( "ReSharper", "FieldCanBeMadeReadOnly.Global" )]
  [StructLayout( LayoutKind.Sequential )]
  public struct SIZE
  {
    public int cx;
    public int cy;
  }

}
