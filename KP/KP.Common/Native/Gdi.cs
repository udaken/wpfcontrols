﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.InteropServices;
using System.Security;
using System.Text;

namespace System.Common.Native
{
  [SuppressUnmanagedCodeSecurity]
  public static class Gdi
  {
    private const string GDI32 = "gdi32";

    public const int PHYSICALOFFSETX = 112;
    public const int PHYSICALOFFSETY = 113;

    [DllImport( GDI32 )]
    public static extern Int32 GetDeviceCaps( IntPtr hdc, Int32 capindex );

  }
}
