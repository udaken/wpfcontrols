﻿// Solution: HW
// Project: HW.Common
// FileName: HWND.cs
// Created: 2015.04.02 9:58
// Last format: 2015.04.02 10:06
// UserName: kp

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace System.Common.Native
{
  public static class HWND
  {
    public static readonly IntPtr
      BOTTOM = new IntPtr( 1 ),
      BROADCAST = new IntPtr( 0xffff ),
      NOTOPMOST = new IntPtr( -2 ),
      TOP = new IntPtr( 0 ),
      TOPMOST = new IntPtr( -1 );
  }
}
