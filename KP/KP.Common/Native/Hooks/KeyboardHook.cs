﻿using System;
using System.Collections.Generic;
using System.Common.Native.Data;
using System.Linq;
using System.Text;

namespace System.Common.Native.Hooks
{
  public class KeyboardHook : SystemHook<KBDLLHOOKSTRUCT>
  {
    private static readonly KeyboardHook _instance = new KeyboardHook();
    public static KeyboardHook Instance { get { return _instance; } }

    private KeyboardHook()
    {
    }

    public override HookType HookType
    {
      get { return HookType.WH_KEYBOARD_LL; }
    }

    internal override HookParamType Paramtype
    {
      get { return HookParamType.ConvertToStruct; }
    }
  }
}
