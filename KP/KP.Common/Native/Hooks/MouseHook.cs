﻿using System;
using System.Collections.Generic;
using System.Common.Native.Data;
using System.Linq;
using System.Text;

namespace System.Common.Native.Hooks
{
  public class MouseHook : SystemHook<MSLLHOOKSTRUCT>
  {
    private static readonly MouseHook _instance = new MouseHook();
    public static MouseHook Instance { get { return _instance; } }

    private MouseHook()
    {
    }

    public override HookType HookType
    {
      get { return HookType.WH_MOUSE_LL; }
    }


    internal override HookParamType Paramtype
    {
      get { return HookParamType.ConvertToStruct; }
    }
  }
}
