﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.InteropServices;
using System.Security;
using System.Text;

#if STARTER
namespace Shell.Native
#else
namespace System.Common.Native
#endif
{
  [SuppressUnmanagedCodeSecurity]
  public static class Kernel
  {
    private const string KERNEL = "kernel32";

    [DllImport( KERNEL, CharSet = CharSet.Auto, SetLastError = true )]
    public static extern IntPtr GetModuleHandle( string lpModuleName );

    #region Console management

    [DllImport( KERNEL )]
    public static extern bool AllocConsole();

    [DllImport( KERNEL )]
    public static extern bool FreeConsole();

    [DllImport( KERNEL )]
    public static extern IntPtr GetConsoleWindow();

    [DllImport( KERNEL )]
    public static extern int GetConsoleOutputCP();

    #endregion

    #region Dynamic load external native dll and function

    [DllImport( "kernel32.dll", EntryPoint = "LoadLibrary" )]
    public static extern IntPtr LoadLibrary( [MarshalAs( UnmanagedType.LPStr )] string lpLibFileName );

    [DllImport( "kernel32.dll", EntryPoint = "GetProcAddress" )]
    public static extern IntPtr GetProcAddress( IntPtr hModule, [MarshalAs( UnmanagedType.LPStr )] string lpProcName );

    [DllImport( "kernel32.dll", EntryPoint = "FreeLibrary" )]
    public static extern bool FreeLibrary( IntPtr hModule );

    #endregion
  }
}
