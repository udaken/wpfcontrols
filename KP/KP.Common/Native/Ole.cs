﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.InteropServices;
using System.Runtime.InteropServices.ComTypes;
using System.Security;
using System.Text;

namespace System.Common.Native
{
  [SuppressUnmanagedCodeSecurity]
  public static class Ole
  {
    private const string OLE32 = "ole32";

    [DllImport( OLE32 )]
    public static extern void ReleaseStgMedium( ref STGMEDIUM pmedium );

  }
}
