﻿// Solution: HW
// Project: HW.Common
// FileName: SetWindowPosFlags.cs
// Created: 2015.04.02 10:06
// Last format: 2015.04.02 10:06
// UserName: kp

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace System.Common.Native
{
  public static class SetWindowPosFlags
  {
    public static readonly uint
      NOSIZE = 0x0001,
      NOMOVE = 0x0002,
      NOZORDER = 0x0004,
      NOREDRAW = 0x0008,
      NOACTIVATE = 0x0010,
      DRAWFRAME = 0x0020,
      FRAMECHANGED = 0x0020,
      SHOWWINDOW = 0x0040,
      HIDEWINDOW = 0x0080,
      NOCOPYBITS = 0x0100,
      NOOWNERZORDER = 0x0200,
      NOREPOSITION = 0x0200,
      NOSENDCHANGING = 0x0400,
      DEFERERASE = 0x2000,
      ASYNCWINDOWPOS = 0x4000;
  }
}
