﻿using System;
using System.Collections.Generic;
using System.Common.Native.Data;
using System.Linq;
using System.Runtime.InteropServices;
using System.Security;
using System.Text;

namespace System.Common.Native
{
  [SuppressUnmanagedCodeSecurity]
  public static class Shell
  {
    #region shell32

    private const string SHELL = "shell32";

    [DllImport( SHELL, CallingConvention = CallingConvention.StdCall )]
    public static extern uint SHAppBarMessage( int dwMessage, ref APPBARDATA pData );

    #endregion

  }
}
