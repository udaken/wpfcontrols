﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.InteropServices;
using System.Runtime.InteropServices.ComTypes;
using System.Security;
using System.Text;

namespace System.Common.Native
{
  [SuppressUnmanagedCodeSecurity]
  public static class Urlmon
  {
    private const string URLMON = "urlmon";

    [DllImport( URLMON )]
    public static extern int CopyStgMedium( ref STGMEDIUM pcstgmedSrc, ref STGMEDIUM pstgmedDest );

  }
}
