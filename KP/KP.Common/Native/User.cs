﻿// Solution: HW
// Project: HW.Common
// FileName: User.cs
// Created: 2014.03.18 8:42
// Last format: 2015.04.02 10:17
// UserName: kp

using System;
using System.Collections.Generic;
using System.Common.Native.Data;
using System.Linq;
using System.Runtime.InteropServices;
using System.Security;
using System.Text;
using System.Windows;
using System.Windows.Interop;

namespace System.Common.Native
{
  [SuppressUnmanagedCodeSecurity]
  public static class User
  {
    #region  ... enums statics fields and  constants...

    private const string USER = "user32";

    #endregion

    #region ...public methods...

    [DllImport( USER )]
    public static extern IntPtr GetMessageExtraInfo();

    [DllImport( USER, CharSet = CharSet.Auto )]
    public static extern bool GetCursorPos( out POINT pt );

    [DllImport( USER, CharSet = CharSet.Auto )]
    public static extern Int32 GetWindowLong( IntPtr hWnd, Int32 nIndex );

    [DllImport( USER, CharSet = CharSet.Auto )]
    public static extern Int32 SetWindowLong( IntPtr hWnd, Int32 nIndex, Int32 newVal );

    [DllImport( USER )]
    public static extern int FindWindow( string className, string windowName );

    [DllImport( USER )]
    public static extern int SetForegroundWindow( IntPtr hWnd );

    [DllImport( USER )]
    public static extern int BringWindowToTop( IntPtr hWnd );

    [DllImport( USER )]
    public static extern IntPtr GetActiveWindow();

    [DllImport( USER )]
    public static extern IntPtr GetTopWindow( IntPtr hWnd );

    [DllImport( USER, CharSet = CharSet.Auto, ExactSpelling = true, CallingConvention = CallingConvention.Winapi )]
    public static extern short GetKeyState( int keyCode );

    [DllImport( USER )]
    public static extern int GetSystemMetrics( int index );

    [DllImport( USER, ExactSpelling = true, CharSet = CharSet.Auto )]
    public static extern bool MoveWindow( IntPtr hWnd, int x, int y, int width, int height, bool repaint );

    [DllImport( USER, CharSet = CharSet.Auto )]
    public static extern int RegisterWindowMessage( string msg );

    [DllImport( USER, CharSet = CharSet.Auto, ExactSpelling = true )]
    public static extern IntPtr GetCapture();

    [DllImport( USER )]
    public static extern bool ClientToScreen( IntPtr hWnd, ref POINT lpPoint );

    [DllImport( USER, SetLastError = true )]
    public static extern bool SetWindowPos( IntPtr hWnd, IntPtr hWndInsertAfter, int left, int top, int width, int height,
                                            uint uFlags );

    #endregion

    #region ... wrappers for Window parameter...

    public static Int32 GetWindowLong( Window window, Int32 index )
    {
      if ( !window.IsVisible )
        return 0;

      var wih = new WindowInteropHelper( window );
      return GetWindowLong( wih.EnsureHandle(), index );
    }

    public static Int32 SetWindowLong( Window window, Int32 nIndex, Int32 newVal )
    {
      if ( !window.IsVisible )
        return 0;

      var wih = new WindowInteropHelper( window );
      return SetWindowLong( wih.EnsureHandle(), nIndex, newVal );
    }

    public static int SetForegroundWindow( Window window )
    {
      if ( !window.IsVisible )
        return 0;

      var wih = new WindowInteropHelper( window );
      return SetForegroundWindow( wih.EnsureHandle() );
    }

    public static int BringWindowToTop( Window window )
    {
      if ( !window.IsVisible )
        return 0;

      var wih = new WindowInteropHelper( window );
      return BringWindowToTop( wih.EnsureHandle() );
    }

    public static IntPtr GetTopWindow( Window window )
    {
      if ( !window.IsVisible )
        return IntPtr.Zero;

      var wih = new WindowInteropHelper( window );
      return GetTopWindow( wih.EnsureHandle() );
    }

    public static bool MoveWindow( Window window, int x, int y, int width, int height, bool repaint )
    {
      if ( !window.IsVisible )
        return false;

      var wih = new WindowInteropHelper( window );
      return MoveWindow( wih.EnsureHandle(), x, y, width, height, repaint );
    }

    public static bool ClientToScreen( Window window, ref POINT lpPoint )
    {
      if ( !window.IsVisible )
        return false;

      var wih = new WindowInteropHelper( window );
      return ClientToScreen( wih.EnsureHandle(), ref lpPoint );
    }

    public static bool SetWindowPos( Window window, IntPtr hWndInsertAfter, int left, int top, int width, int height, uint uFlags )
    {
      if ( !window.IsVisible )
        return false;

      var wih = new WindowInteropHelper( window );
      return SetWindowPos( wih.EnsureHandle(), hWndInsertAfter, left, top, width, height, uFlags );
    }

    #endregion
  }
}
