﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.InteropServices;
using System.Security;
using System.Text;

namespace System.Common.Native
{
  public delegate int HookProc( int nCode, IntPtr wParam, IntPtr lParam );

  [SuppressUnmanagedCodeSecurity]
  public static class UserHooks
  {
    private const string USER = "user32";

    [DllImport( USER, EntryPoint = "SetWindowsHookExA", CharSet = CharSet.Ansi, SetLastError = true, ExactSpelling = true )]
    public static extern IntPtr SetWindowsHookEx( int idHook, HookProc lpfn, IntPtr hMod, int dwThreadId );

    [DllImport( USER, EntryPoint = "UnhookWindowsHookEx", CharSet = CharSet.Ansi, SetLastError = true, ExactSpelling = true )]
    [return: MarshalAs( UnmanagedType.Bool )]
    public static extern bool UnhookWindowsHookEx( IntPtr hHook );

    [DllImport( USER, EntryPoint = "CallNextHookEx", CharSet = CharSet.Ansi, SetLastError = true, ExactSpelling = true )]
    public static extern int CallNextHookEx( IntPtr hHook, int nCode, IntPtr wParam, IntPtr lParam );

  }
}
