﻿using System;
using System.Collections.Generic;
using System.Common.Native.Data;
using System.Linq;
using System.Runtime.InteropServices;
using System.Security;
using System.Text;

namespace System.Common.Native
{
  [SuppressUnmanagedCodeSecurity]
  public static class WinSpool
  {
    private const string WINSPOOL = "winspool.drv";

    [DllImport( WINSPOOL, EntryPoint = "OpenPrinterA", SetLastError = true, CharSet = CharSet.Ansi, ExactSpelling = true, CallingConvention = CallingConvention.StdCall )]
    public static extern bool OpenPrinter( [MarshalAs( UnmanagedType.LPStr )] string szPrinter, out IntPtr hPrinter, IntPtr pd );

    [DllImport( WINSPOOL, EntryPoint = "ClosePrinter", SetLastError = true, ExactSpelling = true, CallingConvention = CallingConvention.StdCall )]
    public static extern bool ClosePrinter( IntPtr hPrinter );

    [DllImport( WINSPOOL, EntryPoint = "StartDocPrinterA", SetLastError = true, CharSet = CharSet.Ansi, ExactSpelling = true, CallingConvention = CallingConvention.StdCall )]
    public static extern bool StartDocPrinter( IntPtr hPrinter, Int32 level, [In, MarshalAs( UnmanagedType.LPStruct )] DOCINFOA di );

    [DllImport( WINSPOOL, EntryPoint = "EndDocPrinter", SetLastError = true, ExactSpelling = true, CallingConvention = CallingConvention.StdCall )]
    public static extern bool EndDocPrinter( IntPtr hPrinter );

    [DllImport( WINSPOOL, EntryPoint = "WritePrinter", SetLastError = true, ExactSpelling = true, CallingConvention = CallingConvention.StdCall )]
    public static extern bool WritePrinter( IntPtr hPrinter, IntPtr pBytes, Int32 dwCount, out Int32 dwWritten );

    [DllImport( WINSPOOL, EntryPoint = "AbortPrinter", SetLastError = true, ExactSpelling = true, CallingConvention = CallingConvention.StdCall )]
    public static extern bool AbortPrinter( IntPtr hPrinter );
  }
}
