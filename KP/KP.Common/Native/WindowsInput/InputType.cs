﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace System.Common.Native.WindowsInput
{
#pragma warning disable 3009

  public enum InputType : uint
  {
    MOUSE = 0,
    KEYBOARD = 1,
    HARDWARE = 2,
  }

#pragma warning restore 3009
}
