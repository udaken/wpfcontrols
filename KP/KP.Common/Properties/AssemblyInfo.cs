﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Runtime.InteropServices;
using System.Text;
using System.Windows.Markup;
// General Information about an assembly is controlled through the following 
// set of attributes. Change these attribute values to modify the information
// associated with an assembly.

[assembly: AssemblyTitle("KP.Common")]
[assembly: AssemblyDescription( "Common classes & interfaces & data structures" )]
[assembly: AssemblyConfiguration("")]
[assembly: AssemblyCompany( "KP" )]
[assembly: AssemblyProduct("KP.Common")]
[assembly: AssemblyCopyright( "Copyright ©  KP 2015" )]
[assembly: AssemblyTrademark("")]
[assembly: AssemblyCulture("")]

// Setting ComVisible to false makes the types in this assembly not visible 
// to COM components.  If you need to access a type in this assembly from 
// COM, set the ComVisible attribute to true on that type.
[assembly: ComVisible(false)]

// The following GUID is for the ID of the typelib if this project is exposed to COM
[assembly: Guid("7b9ed16b-e411-4314-b820-c04c356d2ecd")]

// Version information for an assembly consists of the following four values:
//
//      Major Version
//      Minor Version 
//      Build Number
//      Revision
//
// You can specify all the values or you can default the Build and Revision Numbers 
// by using the '*' as shown below:
// [assembly: AssemblyVersion("1.0.*")]
[assembly: AssemblyVersion("1.0.0.0")]
[assembly: AssemblyFileVersion("1.0.0.0")]

[assembly: XmlnsDefinition( "urn:common", "System.Common" )]
[assembly: XmlnsDefinition( "urn:common-lock", "System.Common.Lock" )]
