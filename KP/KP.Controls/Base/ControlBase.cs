﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;

namespace System.Controls.Base
{
  public class ControlBase : Control
  {
    public static readonly DependencyProperty CornerRadiusProperty =
      DependencyProperty.Register( "CornerRadius",
        typeof( CornerRadius ), typeof( ControlBase ) );

    public CornerRadius CornerRadius
    {
      get { return (CornerRadius)GetValue( CornerRadiusProperty ); }
      set { SetValue( CornerRadiusProperty, value ); }
    }
  }
}
