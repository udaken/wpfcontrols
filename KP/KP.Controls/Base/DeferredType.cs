﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace System.Controls.Base
{
  [Flags]
  public enum DeferredType
  {
    None = 0,
    Index = 1,
    Item = 2,
    Value = 4,
    Selection = Index | Item | Value,

    ValuePath = 8
  };
}
