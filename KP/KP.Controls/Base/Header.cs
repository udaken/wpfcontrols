﻿
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;

namespace System.Controls.Base
{
  public class Header : DependencyObject
  {
    private static readonly DependencyPropertyKey _hasContentPropertyKey =
      DependencyProperty.RegisterAttachedReadOnly(
        "HasContent", typeof( bool ), typeof( Header ),
        new FrameworkPropertyMetadata( false, FrameworkPropertyMetadataOptions.None ) );

    public static readonly DependencyProperty HasContentProperty =
      _hasContentPropertyKey.DependencyProperty;

    public static readonly DependencyProperty ContentProperty =
      DependencyProperty.RegisterAttached( "Content",
        typeof( object ), typeof( Header ),
        new PropertyMetadata( null, ( sender, args )=>SetHasContent( sender, args.NewValue != null ) ) );

    public static readonly DependencyProperty StringFormatProperty =
      DependencyProperty.RegisterAttached( "StringFormat",
        typeof( string ), typeof( Header ), new PropertyMetadata( string.Empty ) );

    public static readonly DependencyProperty TemplateProperty =
      DependencyProperty.RegisterAttached( "Template",
        typeof( DataTemplate ), typeof( Header ), new PropertyMetadata( null ) );

    public static readonly DependencyProperty TemplateSelectorProperty =
      DependencyProperty.RegisterAttached( "TemplateSelector",
        typeof( DataTemplateSelector ), typeof( Header ), new PropertyMetadata( null ) );

    public static int GetHasContent( DependencyObject obj )
    {
      return (int)obj.GetValue( HasContentProperty );
    }

    internal static void SetHasContent( DependencyObject obj, bool value )
    {
      obj.SetValue( _hasContentPropertyKey, value );
    }

    public static object GetContent( DependencyObject obj )
    {
      return obj.GetValue( ContentProperty );
    }

    public static void SetContent( DependencyObject obj, object value )
    {
      obj.SetValue( ContentProperty, value );
    }

    public static string GetStringFormat( DependencyObject obj )
    {
      return (string)obj.GetValue( StringFormatProperty );
    }

    public static void SetStringFormat( DependencyObject obj, string value )
    {
      obj.SetValue( StringFormatProperty, value );
    }

    public static DataTemplate GetTemplate( DependencyObject obj )
    {
      return (DataTemplate)obj.GetValue( TemplateProperty );
    }

    public static void SetTemplate( DependencyObject obj, DataTemplate value )
    {
      obj.SetValue( TemplateProperty, value );
    }

    public static DataTemplateSelector GetTemplateSelector( DependencyObject obj )
    {
      return (DataTemplateSelector)obj.GetValue( TemplateSelectorProperty );
    }

    public static void SetTemplateSelector( DependencyObject obj, DataTemplateSelector value )
    {
      obj.SetValue( TemplateSelectorProperty, value );
    }
  }
}
