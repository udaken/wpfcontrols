﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Controls;
using System.Windows.Controls.Primitives;

namespace System.Controls.Base
{
  internal interface  ISelectionOwner
  {
    GeneratorStatus GetGeneratorStatus();

    int ItemsCount { get; }
    ItemCollection Items { get; }

    void SetDeferred( object value, DeferredType dType );
    void SetSelectedIndex( int newIndex );
    void SetSelectedItem( object newValue );
  }
}
