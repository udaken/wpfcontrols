﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;

namespace System.Controls.Base
{
  public class LabelBase : Label
  {
    public static readonly DependencyProperty CornerRadiusProperty =
      DependencyProperty.Register( "CornerRadius",
        typeof( CornerRadius ), typeof( LabelBase ) );

    public CornerRadius CornerRadius
    {
      get { return (CornerRadius)GetValue( CornerRadiusProperty ); }
      set { SetValue( CornerRadiusProperty, value ); }
    }
  }
}
