﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls.Primitives;

namespace System.Controls.Base
{
  public class Selection : DependencyObject
  {
    #region ... SelectedItem ...

    public static readonly DependencyProperty SelectedItemProperty =
      DependencyProperty.RegisterAttached( "SelectedItem",
        typeof( object ), typeof( Selection ),
        new FrameworkPropertyMetadata(
          null, FrameworkPropertyMetadataOptions.BindsTwoWayByDefault,
          OnSelectedItemChanged, CoerceSelectedItem ) );

    public static object GetSelectedItem( DependencyObject obj )
    {
      return obj.GetValue( SelectedItemProperty );
    }

    public static void SetSelectedItem( DependencyObject obj, object value )
    {
      obj.SetValue( SelectedItemProperty, value );
    }

    private static ISelectionOwner GetOwner( DependencyObject dep )
    {
      return dep as ISelectionOwner;
    }

    private static void OnSelectedItemChanged( DependencyObject dep, DependencyPropertyChangedEventArgs arg )
    {
      var so = GetOwner( dep );

      if ( so != null )
        so.SetSelectedItem( arg.NewValue );
    }

    private static object CoerceSelectedItem( DependencyObject dep, object value )
    {
      var so = dep as ISelectionOwner;
      if ( so == null )
        return null;

      if ( so.GetGeneratorStatus() != GeneratorStatus.ContainersGenerated )
        so.SetDeferred( value, DeferredType.Item );

      var selectedIndex = GetSelectedIndex( dep );

      if ( ( selectedIndex > -1 && selectedIndex < so.ItemsCount && so.Items[selectedIndex] == value ) || so.Items.Contains( value ) )
        return value;

      return DependencyProperty.UnsetValue;
    }

    #endregion

    #region ... SelectedIndex ...

    public static readonly DependencyProperty SelectedIndexProperty =
      DependencyProperty.RegisterAttached( "SelectedIndex",
        typeof( int ), typeof( Selection ),
        new FrameworkPropertyMetadata(
          -1, FrameworkPropertyMetadataOptions.BindsTwoWayByDefault | FrameworkPropertyMetadataOptions.Journal,
          OnSelectedIndexChanged, CoerceSelectedIndex ), ValidateSelectedIndex );

    public static int GetSelectedIndex( DependencyObject obj )
    {
      return (int) obj.GetValue( SelectedIndexProperty );
    }

    public static void SetSelectedIndex( DependencyObject obj, int value )
    {
      obj.SetValue( SelectedIndexProperty, value );
    }

    private static bool ValidateSelectedIndex( object value )
    {
      return ( (int) value ) >= -1;
    }

    private static object CoerceSelectedIndex( DependencyObject dep, object value )
    {
      var so = GetOwner( dep );
      if ( so == null )
        return null;

      if ( so.GetGeneratorStatus() != GeneratorStatus.ContainersGenerated )
      {
        if ( ( value is int ) )
          so.SetDeferred( value, DeferredType.Index );
      }

      if ( ( value is int ) && (int) value >= so.ItemsCount )
        return DependencyProperty.UnsetValue;

      return value;
    }

    private static void OnSelectedIndexChanged( DependencyObject dep, DependencyPropertyChangedEventArgs arg )
    {
      var so = GetOwner( dep );
      if ( so == null )
        return;

      var newIndex = (int) arg.NewValue;

      so.SetSelectedIndex( newIndex );
    }

    #endregion

    #region ... SelectedValue... ...

    public static readonly DependencyProperty SelectedValuePathProperty =
      DependencyProperty.RegisterAttached( "SelectedValuePath",
        typeof( string ), typeof( Selection ), new PropertyMetadata( string.Empty ) );

    public static readonly DependencyProperty SelectedValueProperty =
      DependencyProperty.RegisterAttached( "SelectedValue",
        typeof( object ), typeof( Selection ), new PropertyMetadata( null ) );

    public static string GetSelectedValuePath( DependencyObject obj )
    {
      return (string) obj.GetValue( SelectedValuePathProperty );
    }

    public static void SetSelectedValuePath( DependencyObject obj, string value )
    {
      obj.SetValue( SelectedValuePathProperty, value );
    }

    public static object GetSelectedValue( DependencyObject obj )
    {
      return obj.GetValue( SelectedValueProperty );
    }

    public static void SetSelectedValue( DependencyObject obj, object value )
    {
      obj.SetValue( SelectedValueProperty, value );
    }

    #endregion
  }
}
