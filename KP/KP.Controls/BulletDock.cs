﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace System.Controls
{
  public enum BulletDock
  {
    Left,
    Right,
    Top,
    Bottom
  }
}
