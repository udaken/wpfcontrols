﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;

namespace System.Controls
{
  public class CheckBox : System.Windows.Controls.CheckBox
  {
    
    public static readonly DependencyProperty CheckAlignProperty =
      DependencyProperty.Register( "CheckAlign",
        typeof( CheckAlign ), typeof( CheckBox ) );

    public static readonly DependencyProperty SharedSizeGroupProperty =
      DependencyProperty.Register( "SharedSizeGroup",
        typeof( string ), typeof( CheckBox ),
        new PropertyMetadata( "CheckBoxContent" ) );

    public static readonly DependencyProperty TextWrappingProperty =
      DependencyProperty.Register( "TextWrapping",
        typeof( TextWrapping ), typeof( CheckBox ) );

    public static readonly DependencyProperty TextWidthProperty =
      DependencyProperty.Register( "TextWidth",
        typeof( double ), typeof( CheckBox ),
        new PropertyMetadata( double.NaN ) );

    static CheckBox()
    {
      DefaultStyleKeyProperty.OverrideMetadata( typeof( CheckBox ),
        new FrameworkPropertyMetadata( typeof( CheckBox ) ) );
    }

    public CheckAlign CheckAlign
    {
      get { return (CheckAlign) GetValue( CheckAlignProperty ); }
      set { SetValue( CheckAlignProperty, value ); }
    }

    public string SharedSizeGroup
    {
      get { return (string) GetValue( SharedSizeGroupProperty ); }
      set { SetValue( SharedSizeGroupProperty, value ); }
    }

    public TextWrapping TextWrapping
    {
      get { return (TextWrapping) GetValue( TextWrappingProperty ); }
      set { SetValue( TextWrappingProperty, value ); }
    }

    public double TextWidth
    {
      get { return (double) GetValue( TextWidthProperty ); }
      set { SetValue( TextWidthProperty, value ); }
    }
  }
}
