﻿using System;
using System.Collections.Generic;
using System.Common.Lock;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Input;
using System.Windows.Threading;

namespace System.Controls.Command
{
  public static class InternalCommandManager
  {
    private static readonly ObservableMonitor _commandMonitor = new ObservableMonitor();

    static InternalCommandManager()
    {
      _commandMonitor.Subscribe( BusyRestored );
    }

    private static void BusyRestored( object sender, EventArgs eventArgs )
    {
      Application.Current.Dispatcher.BeginInvoke( (Action) ( CommandManager.InvalidateRequerySuggested ), DispatcherPriority.Background );
    }

    public static IDisposable DisableCommands()
    {
      _commandMonitor.Enter();
      CommandManager.InvalidateRequerySuggested();

      return _commandMonitor;
    }

    public static bool IsCommandEnabled
    {
      get { return !_commandMonitor.Busy; }
    }
  }
}
