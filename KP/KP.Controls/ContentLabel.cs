﻿using System;
using System.Collections.Generic;
using System.Controls.Base;
using System.Linq;
using System.Text;
using System.Windows;

namespace System.Controls
{
  public class ContentLabel : LabelBase
  {
    static ContentLabel()
    {
      DefaultStyleKeyProperty.OverrideMetadata( typeof( ContentLabel ),
        new FrameworkPropertyMetadata( typeof( ContentLabel ) ) );
    }
  }
}
