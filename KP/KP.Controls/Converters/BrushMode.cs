﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace System.Controls.Converters
{
  public enum BrushMode { Glass, Saturation, Hue, Luminance, Reverse, GrayScale };
}
