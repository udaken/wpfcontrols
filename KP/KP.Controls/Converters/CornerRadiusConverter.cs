﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Data;

namespace System.Controls.Converters
{
  public class CornerRadiusConverter : IValueConverter
  {
    #region Implementation of IValueConverter

    public object Convert( object value, Type targetType, object parameter, CultureInfo culture )
    {
      if ( !( value is CornerRadius ) || parameter == null )
        return value;

      var cornerRadius = (CornerRadius) value;
      var positions = parameter.ToString().Split( new[]
                                                  {
                                                    ','
                                                  }, StringSplitOptions.RemoveEmptyEntries );
      var index = 0;
      foreach ( var position in positions )
      {
        if ( index == 0 && position == "0" )
          cornerRadius.TopLeft = 0;

        if ( index == 1 && position == "0" )
          cornerRadius.TopRight = 0;

        if ( index == 2 && position == "0" )
          cornerRadius.BottomRight = 0;

        if ( index == 3 && position == "0" )
          cornerRadius.BottomLeft = 0;

        index++;
      }

      return cornerRadius;
    }

    public object ConvertBack( object value, Type targetType, object parameter, CultureInfo culture )
    {
      return null;
    }

    #endregion
  }
}
