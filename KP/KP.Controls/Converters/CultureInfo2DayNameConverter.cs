﻿using System;
using System.Collections.Generic;
using System.Common;
using System.Controls.DateTime;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Windows.Data;

namespace System.Controls.Converters
{
  public class CultureInfo2DayNameConverter : IValueConverter
  {
    #region Implementation of IValueConverter

    public object Convert( object value, Type targetType, object parameter, CultureInfo culture )
    {
      var sourceInfo = value as CultureInfo;
      if ( sourceInfo == null )
        sourceInfo = CultureInfo.CurrentCulture;

      var mask = Converter.ConvertTo<SetOfDays>( parameter );
      switch ( mask )
      {
      case SetOfDays.None:
        return null;

      case SetOfDays.Sunday:
        return sourceInfo.DateTimeFormat.DayNames[0];

      case SetOfDays.Monday:
        return sourceInfo.DateTimeFormat.DayNames[1];

      case SetOfDays.Tuesday:
        return sourceInfo.DateTimeFormat.DayNames[2];

      case SetOfDays.Wednesday:
        return sourceInfo.DateTimeFormat.DayNames[3];

      case SetOfDays.Thursday:
        return sourceInfo.DateTimeFormat.DayNames[4];

      case SetOfDays.Friday:
        return sourceInfo.DateTimeFormat.DayNames[5];

      case SetOfDays.Saturday:
        return sourceInfo.DateTimeFormat.DayNames[6];
        
      }

      return null;
    }

    public object ConvertBack( object value, Type targetType, object parameter, CultureInfo culture )
    {
      return null;
    }

    #endregion
  }
}
