﻿using System;
using System.Collections.Generic;
using System.Common;
using System.Controls.DateTime;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Windows.Data;

namespace System.Controls.Converters
{
  public class CultureInfo2MonthNameConverter : IValueConverter
  {
    #region Implementation of IValueConverter

    public object Convert( object value, Type targetType, object parameter, CultureInfo culture )
    {
      var sourceInfo = value as CultureInfo;
      if ( sourceInfo == null )
        sourceInfo = CultureInfo.CurrentCulture;

      var mask = Converter.ConvertTo<SetOfMonths>( parameter );
      switch ( mask )
      {
      case SetOfMonths.None:
        return null;

      case SetOfMonths.January:
        return sourceInfo.DateTimeFormat.MonthNames[0];

      case SetOfMonths.February:
        return sourceInfo.DateTimeFormat.MonthNames[1];

      case SetOfMonths.March:
        return sourceInfo.DateTimeFormat.MonthNames[2];

      case SetOfMonths.April:
        return sourceInfo.DateTimeFormat.MonthNames[3];

      case SetOfMonths.May:
        return sourceInfo.DateTimeFormat.MonthNames[4];

      case SetOfMonths.June:
        return sourceInfo.DateTimeFormat.MonthNames[5];

      case SetOfMonths.July:
        return sourceInfo.DateTimeFormat.MonthNames[6];

      case SetOfMonths.August:
        return sourceInfo.DateTimeFormat.MonthNames[7];

      case SetOfMonths.September:
        return sourceInfo.DateTimeFormat.MonthNames[8];

      case SetOfMonths.October:
        return sourceInfo.DateTimeFormat.MonthNames[9];

      case SetOfMonths.November:
        return sourceInfo.DateTimeFormat.MonthNames[10];

      case SetOfMonths.December:
        return sourceInfo.DateTimeFormat.MonthNames[11];
      }

      return null;
    }

    public object ConvertBack( object value, Type targetType, object parameter, CultureInfo culture )
    {
      return null;
    }

    #endregion
  }
}