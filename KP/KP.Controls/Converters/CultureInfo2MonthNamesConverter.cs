﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Windows.Data;

namespace System.Controls.Converters
{
  public class CultureInfo2MonthNamesConverter : IValueConverter
  {
    #region Implementation of IValueConverter

    public object Convert( object value, Type targetType, object parameter, CultureInfo culture )
    {
      var cultureSource = value as CultureInfo;

      if ( cultureSource == null )
        cultureSource = CultureInfo.CurrentCulture;

      return cultureSource.DateTimeFormat.MonthNames;
    }

    public object ConvertBack( object value, Type targetType, object parameter, CultureInfo culture )
    {
      return null;
    }

    #endregion
  }
}
