﻿using System;
using System.Collections.Generic;
using System.Controls.DateTime;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Windows.Data;

namespace System.Controls.Converters
{
  public class MonthName2SetOfMonthsConverter : IValueConverter
  {
    #region Implementation of IValueConverter

    public object Convert( object value, Type targetType, object parameter, CultureInfo culture )
    {
      var index = CultureInfo.CurrentCulture.DateTimeFormat.DayNames.ToList().IndexOf( value.ToString() );
      var set = 1 << index;

      return (SetOfMonths) set;
    }

    public object ConvertBack( object value, Type targetType, object parameter, CultureInfo culture )
    {
      return null;
    }

    #endregion
  }
}