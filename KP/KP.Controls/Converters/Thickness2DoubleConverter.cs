﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Data;

namespace System.Controls.Converters
{
  public class Thickness2DoubleConverter : IValueConverter
  {
    #region Implementation of IValueConverter

    public object Convert( object value, Type targetType, object parameter, CultureInfo culture )
    {
      if ( !( value is Thickness ) || parameter == null )
        return value;

      var thickness = (Thickness)value;
      var positions = parameter.ToString().Split( new[]
                                                  {
                                                    ','
                                                  }, StringSplitOptions.RemoveEmptyEntries );
      var index = 0;
      foreach ( var position in positions )
      {
        if ( index == 0 && position == "0" )
          return thickness.Left;

        if ( index == 1 && position == "0" )
          return thickness.Top;

        if ( index == 2 && position == "0" )
          return thickness.Right;

        if ( index == 3 && position == "0" )
          return thickness.Bottom;

        index++;
      }

      return null;
    }

    public object ConvertBack( object value, Type targetType, object parameter, CultureInfo culture )
    {
      return null;
    }

    #endregion
  }
}
