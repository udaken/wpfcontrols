﻿using System;
using System.Collections.Generic;
using System.Controls.Base;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Windows;

namespace System.Controls.DateTime
{
  public class CultureBasedControlBase : ControlBase
  {
    public static readonly DependencyProperty CultureProperty =
      DependencyProperty.Register( "Culture",
        typeof( CultureInfo ), typeof( CultureBasedControlBase ),
        new PropertyMetadata( CultureInfo.CurrentCulture ) );

    public CultureInfo Culture
    {
      get { return (CultureInfo)GetValue( CultureProperty ); }
      set { SetValue( CultureProperty, value ); }
    }
  }
}
