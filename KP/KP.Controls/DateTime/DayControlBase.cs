﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;

namespace System.Controls.DateTime
{
  public class DayControlBase : CultureBasedControlBase
  {
    public static readonly DependencyProperty DaysProperty =
      DependencyProperty.Register( "Days",
        typeof( SetOfDays ), typeof( DayControlBase ),
        new FrameworkPropertyMetadata( SetOfDays.None, FrameworkPropertyMetadataOptions.BindsTwoWayByDefault ) );

    public SetOfDays Days
    {
      get { return (SetOfDays)GetValue( DaysProperty ); }
      set { SetValue( DaysProperty, value ); }
    }
  }
}
