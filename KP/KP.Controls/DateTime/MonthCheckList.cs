﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;

namespace System.Controls.DateTime
{
  public class MonthCheckList : MonthControlBase
  {
    public static readonly DependencyProperty ShowNoneProperty =
      DependencyProperty.Register( "ShowNone",
        typeof( bool ), typeof( MonthCheckList ) );

    static MonthCheckList()
    {
      DefaultStyleKeyProperty.OverrideMetadata( typeof( MonthCheckList ),
        new FrameworkPropertyMetadata( typeof( MonthCheckList ) ) );
    }

    public bool ShowNone
    {
      get { return (bool)GetValue( ShowNoneProperty ); }
      set { SetValue( ShowNoneProperty, value ); }
    }
  }
}
