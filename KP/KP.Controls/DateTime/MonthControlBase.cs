﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;

namespace System.Controls.DateTime
{
  public class MonthControlBase : CultureBasedControlBase
  {
    public static readonly DependencyProperty MonthsProperty =
      DependencyProperty.Register( "Months",
        typeof( SetOfMonths ), typeof( MonthControlBase ),
        new FrameworkPropertyMetadata( SetOfMonths.None, FrameworkPropertyMetadataOptions.BindsTwoWayByDefault ) );

    public SetOfMonths Months
    {
      get { return (SetOfMonths) GetValue( MonthsProperty ); }
      set { SetValue( MonthsProperty, value ); }
    }
  }
}
