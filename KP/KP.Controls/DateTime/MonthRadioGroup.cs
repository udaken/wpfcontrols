﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;

namespace System.Controls.DateTime
{
  public class MonthRadioGroup : MonthControlBase
  {
    static MonthRadioGroup()
    {
      DefaultStyleKeyProperty.OverrideMetadata( typeof( MonthRadioGroup ),
        new FrameworkPropertyMetadata( typeof( MonthRadioGroup ) ) );
    }
  }
}
