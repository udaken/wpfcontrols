﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace System.Controls.DateTime
{
  [Flags]
  public enum SetOfDays
  {
    None = 0,
    Sunday = 1,
    Monday = 2,
    Tuesday = 4,
    Wednesday = 8,
    Thursday = 16,
    Friday = 32,
    Saturday = 64
  }
}
