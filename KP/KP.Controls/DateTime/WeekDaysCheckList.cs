﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;

namespace System.Controls.DateTime
{
  public class WeekDaysCheckList : DayControlBase
  {
    public static readonly DependencyProperty ShowNoneProperty =
      DependencyProperty.Register( "ShowNone",
        typeof( bool ), typeof( WeekDaysCheckList ) );

    static WeekDaysCheckList()
    {
      DefaultStyleKeyProperty.OverrideMetadata( typeof( WeekDaysCheckList ),
        new FrameworkPropertyMetadata( typeof( WeekDaysCheckList ) ) );
    }

    public bool ShowNone
    {
      get { return (bool)GetValue( ShowNoneProperty ); }
      set { SetValue( ShowNoneProperty, value ); }
    }
  }
}
