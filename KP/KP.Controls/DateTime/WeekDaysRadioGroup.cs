﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;

namespace System.Controls.DateTime
{
  public class WeekDaysRadioGroup : DayControlBase
  {
    static WeekDaysRadioGroup()
    {
      DefaultStyleKeyProperty.OverrideMetadata( typeof( WeekDaysRadioGroup ),
        new FrameworkPropertyMetadata( typeof( WeekDaysRadioGroup ) ) );
    }
  }
}
