﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Media;

namespace System.Controls.Extensions
{
  public static class ColorEx
  {
    public static Color FromInt( this Int32 iCol )
    {
      var argb = BitConverter.GetBytes( iCol );

      return Color.FromArgb( argb[0], argb[1], argb[2], argb[3] );
    }

    public static int ToInt( this Color color )
    {
      return BitConverter.ToInt32( new[]
                                   {
                                     color.A,
                                     color.R,
                                     color.G,
                                     color.B
                                   }, 0 );
    }

    public static float GetHue( this Color color )
    {
      var drawColor = System.Drawing.Color.FromArgb( color.A, color.R, color.G, color.B );
      return drawColor.GetHue();
    }

    public static float GetSaturation( this Color color )
    {
      var drawColor = System.Drawing.Color.FromArgb( color.A, color.R, color.G, color.B );
      return drawColor.GetSaturation();
    }

    public static float GetLuminosity( this Color color )
    {
      return color.GetBrightness();
    }

    public static float GetBrightness( this Color color )
    {
      var drawColor = System.Drawing.Color.FromArgb( color.A, color.R, color.G, color.B );
      return drawColor.GetBrightness();
    }

    public static Color AdjustHsl( this Color thisColor, float h, float s, float l )
    {
      var hsl = new Hsl( thisColor );

      hsl.Adjust( h, s, l );

      return hsl.Color;
    }
  }
}
