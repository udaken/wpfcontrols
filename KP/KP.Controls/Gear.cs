﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;

namespace System.Controls
{
  public class Gear : Control
  {
    static Gear()
    {
      DefaultStyleKeyProperty.OverrideMetadata( typeof( Gear ),
        new FrameworkPropertyMetadata( typeof( Gear ) ) );
    }

    public bool Rotate
    {
      get { return (bool)GetValue( RotateProperty ); }
      set { SetValue( RotateProperty, value ); }
    }

    public static readonly DependencyProperty RotateProperty =
      DependencyProperty.Register( "Rotate",
        typeof( bool ), typeof( Gear ),
        new PropertyMetadata( false ) );
  }
}
