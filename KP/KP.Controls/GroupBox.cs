﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;

namespace System.Controls
{
  public class GroupBox : System.Windows.Controls.GroupBox
  {
    public static readonly DependencyProperty CornerRadiusProperty =
      DependencyProperty.Register( "CornerRadius",
        typeof( CornerRadius ), typeof( GroupBox ) );

    static GroupBox()
    {
      DefaultStyleKeyProperty.OverrideMetadata( typeof( GroupBox ),
        new FrameworkPropertyMetadata( typeof( GroupBox ) ) );
    }

    public CornerRadius CornerRadius
    {
      get { return (CornerRadius)GetValue( CornerRadiusProperty ); }
      set { SetValue( CornerRadiusProperty, value ); }
    }
  }
}
