﻿// Solution: HW
// Project: HW.Controls
// FileName: HighlightCollection.cs
// Created: 2015.07.15 17:53
// Last format: 2015.07.15 17:53
// UserName: kp

using System;
using System.Collections;
using System.Collections.Generic;
using System.Common.Highliter;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Media;

namespace System.Controls.Highliter
{
  public class HighlightCollection : IEnumerable<HighlightStyle>
  {
    private readonly SortedList<TokenType, HighlightStyle> fHighlights = new SortedList<TokenType, HighlightStyle>();

    public HighlightCollection()
    {
      fHighlights.Add( TokenType.Comment, new HighlightStyle() { Foreground = Colors.Green, FontStyle = FontStyles.Italic } );
      fHighlights.Add( TokenType.Exception, new HighlightStyle() { Foreground = Colors.Red } );
      fHighlights.Add( TokenType.Function, new HighlightStyle()
      {
        FontSize = (int) ( HighlightStyle.GetDefault().FontSize * 1.25 ),
        Foreground = Colors.Purple,
        FontStyle = FontStyles.Italic,
        FontWeight = FontWeights.Bold
      } );

      fHighlights.Add( TokenType.Datatype, new HighlightStyle()
      {
        Foreground = Colors.SteelBlue,
        FontStyle = FontStyles.Italic
      } );
      fHighlights.Add( TokenType.Domain, new HighlightStyle()
      {
        Foreground = Colors.SteelBlue,
        FontStyle = FontStyles.Italic
      } );

      fHighlights.Add( TokenType.Identifier, new HighlightStyle()
      {
        Foreground = Colors.Maroon
      } );

      fHighlights.Add( TokenType.Variable, new HighlightStyle()
      {
        Foreground = Colors.Maroon
      } );

      fHighlights.Add( TokenType.KeyWord, new HighlightStyle() { Foreground = Colors.Blue, FontWeight = FontWeights.Bold } );
      fHighlights.Add( TokenType.String, new HighlightStyle() { Foreground = Colors.Chocolate, FontStyle = FontStyles.Italic } );
      fHighlights.Add( TokenType.TableName, new HighlightStyle()
      {
        FontSize = (int) ( HighlightStyle.GetDefault().FontSize * 1.25 ),
        Foreground = Colors.Red,
        FontWeight = FontWeights.Bold
      } );

      fHighlights.Add( TokenType.ViewName, new HighlightStyle()
      {
        FontSize = (int) ( HighlightStyle.GetDefault().FontSize * 1.25 ),
        Foreground = Color.FromRgb( 217, 11, 206 ),
        FontStyle = FontStyles.Italic,
        FontWeight = FontWeights.Bold
      } );
      
    }

    public bool TryGetValue( TokenType key, out HighlightStyle value )
    {
      return fHighlights.TryGetValue( key, out value );
    }

    #region Implementation of IEnumerable

    public IEnumerator<HighlightStyle> GetEnumerator()
    {
      return fHighlights.Values.GetEnumerator();
    }

    IEnumerator IEnumerable.GetEnumerator()
    {
      return GetEnumerator();
    }

    #endregion
  }
}