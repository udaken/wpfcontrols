﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;

namespace System.Controls
{
  public class MemoEdit : TextBox
  {
    static MemoEdit()
    {
      DefaultStyleKeyProperty.OverrideMetadata( typeof( MemoEdit ),
        new FrameworkPropertyMetadata( typeof( MemoEdit ) ) );
    }
  }
}
