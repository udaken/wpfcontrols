﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Input;

namespace System.Controls
{
  public class QueryMoveFocusEventArgs : RoutedEventArgs
  {
    internal QueryMoveFocusEventArgs( FocusNavigationDirection direction, bool reachedMaxLength )
      : base( TextBox.QueryMoveFocusEvent )
    {
      FocusNavigationDirection = direction;
      ReachedMaxLength = reachedMaxLength;
      CanMoveFocus = true;
    }

    public FocusNavigationDirection FocusNavigationDirection { get; private set; }
    public bool ReachedMaxLength { get; private set; }

    public bool CanMoveFocus { get; set; }
  }
}