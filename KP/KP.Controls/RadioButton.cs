﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;

namespace System.Controls
{
  public class RadioButton : System.Windows.Controls.RadioButton
  {
    public static readonly DependencyProperty BulletDockProperty =
      DependencyProperty.Register( "BulletDock",
        typeof( BulletDock ), typeof( RadioButton ) );

    public static readonly DependencyProperty SharedSizeGroupProperty =
      DependencyProperty.Register( "SharedSizeGroup",
        typeof( string ), typeof( RadioButton ),
        new PropertyMetadata( "RadioButtonContent" ) );

    public static readonly DependencyProperty TextWrappingProperty =
      DependencyProperty.Register( "TextWrapping",
        typeof( TextWrapping ), typeof( RadioButton ) );

    public static readonly DependencyProperty TextWidthProperty =
      DependencyProperty.Register( "TextWidth",
        typeof( double ), typeof( RadioButton ),
        new PropertyMetadata( double.NaN ) );

    public static readonly DependencyProperty IsAlternateUncheckedProperty =
      DependencyProperty.Register( "IsAlternateUnchecked",
        typeof( bool ), typeof( RadioButton ) );

    static RadioButton()
    {
      DefaultStyleKeyProperty.OverrideMetadata( typeof( RadioButton ),
        new FrameworkPropertyMetadata( typeof( RadioButton ) ) );
    }

    public BulletDock BulletDock
    {
      get { return (BulletDock)GetValue( BulletDockProperty ); }
      set { SetValue( BulletDockProperty, value ); }
    }

    public string SharedSizeGroup
    {
      get { return (string)GetValue( SharedSizeGroupProperty ); }
      set { SetValue( SharedSizeGroupProperty, value ); }
    }

    public TextWrapping TextWrapping
    {
      get { return (TextWrapping)GetValue( TextWrappingProperty ); }
      set { SetValue( TextWrappingProperty, value ); }
    }

    public double TextWidth
    {
      get { return (double)GetValue( TextWidthProperty ); }
      set { SetValue( TextWidthProperty, value ); }
    }

    public bool IsAlternateUnchecked
    {
      get { return (bool)GetValue( IsAlternateUncheckedProperty ); }
      set { SetValue( IsAlternateUncheckedProperty, value ); }
    }
  }
}
