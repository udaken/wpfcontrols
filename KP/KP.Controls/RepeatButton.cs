﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;

namespace System.Controls
{
  public class RepeatButton : System.Windows.Controls.Primitives.RepeatButton
  {
    public static readonly DependencyProperty CornerRadiusProperty =
      DependencyProperty.Register( "CornerRadius",
        typeof( CornerRadius ), typeof( RepeatButton ) );

    static RepeatButton()
    {
      DefaultStyleKeyProperty.OverrideMetadata( typeof( RepeatButton ),
        new FrameworkPropertyMetadata( typeof( RepeatButton ) ) );
    }

    public CornerRadius CornerRadius
    {
      get { return (CornerRadius)GetValue( CornerRadiusProperty ); }
      set { SetValue( CornerRadiusProperty, value ); }
    }
  }
}
