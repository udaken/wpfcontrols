﻿using System;
using System.Collections.Generic;
using System.Controls.Base;
using System.Linq;
using System.Text;
using System.Windows;

namespace System.Controls
{
  public class TextLabel : LabelBase
  {
    static TextLabel()
    {
      DefaultStyleKeyProperty.OverrideMetadata( typeof( TextLabel ),
        new FrameworkPropertyMetadata( typeof( TextLabel ) ) );
    }

    public TextAlignment TextAlignment
    {
      get { return (TextAlignment) GetValue( TextAlignmentProperty ); }
      set { SetValue( TextAlignmentProperty, value ); }
    }

    public static readonly DependencyProperty TextAlignmentProperty =
      DependencyProperty.Register( "TextAlignment",
        typeof( TextAlignment ), typeof( TextLabel ) );

    public TextWrapping TextWrapping
    {
      get { return (TextWrapping)GetValue(TextWrappingProperty); }
      set { SetValue(TextWrappingProperty, value); }
    }

    public static readonly DependencyProperty TextWrappingProperty = 
      DependencyProperty.Register("TextWrapping", 
        typeof(TextWrapping), typeof(TextLabel));



  }
}
