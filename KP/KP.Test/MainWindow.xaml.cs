﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Windows;

namespace KP.Test
{
  /// <summary>
  /// Interaction logic for MainWindow.xaml
  /// </summary>
  public partial class MainWindow
  {

    public MainWindow()
    {
      InitializeComponent();
    }

    public string Sqltext
    {
      get { return (string)GetValue(SqltextProperty); }
      set { SetValue(SqltextProperty, value); }
    }

    public static readonly DependencyProperty SqltextProperty = 
      DependencyProperty.Register("Sqltext", 
        typeof(string), typeof(MainWindow));

    private void ButtonBase_OnClick( object sender, RoutedEventArgs e )
    {
      Sqltext = File.ReadAllText( @"c:\Munka\tmp\ARU_npl_trig.sql", Encoding.Default );

    }
  }
}
